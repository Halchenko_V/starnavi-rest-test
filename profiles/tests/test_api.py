from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from authentication.models import User


class ProfilesTests(APITestCase):
    def test_profile(self):
        User.objects.create_user(
            username="superprogrammer3000",
            email="email@example.com",
            password="Pas$w0rd",
        )
        response = self.client.get(
            reverse("profiles:profile-detail", args=["superprogrammer3000"]),
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                "profile": {
                    "username": "superprogrammer3000",
                    "bio": "",
                    "image": "https://static.productionready.io/images/smiley-cyrus.jpg",
                }
            },
        )
