import json

from rest_framework.renderers import JSONRenderer


class StarnaviJSONRenderer(JSONRenderer):
    charset = "utf-8"
    object_label = "object"

    def render(self, data, media_type=None, renderer_context=None):
        try:
            errors = data.get("errors", None)
        except AttributeError:
            pass
        else:
            if errors is not None:
                return super(StarnaviJSONRenderer, self).render(data)
        return super(StarnaviJSONRenderer, self).render({self.object_label: data})
