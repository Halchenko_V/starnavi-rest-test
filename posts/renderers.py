from core.renderers import StarnaviJSONRenderer


class PostJSONRenderer(StarnaviJSONRenderer):
    object_label = "post"


class PostsJSONRenderer(StarnaviJSONRenderer):
    object_label = "posts"
