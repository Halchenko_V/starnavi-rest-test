## STARNAVI REST API TEST APPLICATION + Email check with emailhunter

[The task](https://drive.google.com/file/d/1gbOd6rOMxSS38dOuKQdGvzwjqoXaUbWR/view)


| Edpoint           | Method       | Meaning  |
| -------------     |:-------------:| -----:|
| `/api/users/`     | `POST`        | Sign up |
| `/api/users/login/`|`POST`      | Sign in |
| `/api/user/` | `PUT`| Change user data (email, username, etc.) of the logged in user|
| `/api/user/`|`GET`| Get info of the current user|
| `/api/profiles/<username>` | `GET` | Public info of a user |
| `/api/posts/`| `GET` | The list view of all posts |
| `/api/posts`| `POST` | Create a new post |
| `/api/post/<post id>`| `GET`| Get post |
| `/api/post/<post id>`| `PUT`| Edit post |
| `/api/post/<post id>`| `PATCH`| Set/unset a like or edit post (if the user is the author)|
| `/api/post/liked/`| `GET` | List of all posts liked by the user|


Please refer to the unittests for more info.

