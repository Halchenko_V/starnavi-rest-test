from django.urls import path

from .views import RegistrationAPIView, LoginAPIView, UserRetrieveUpdateAPIView

app_name = "authentication"

urlpatterns = [
    path("users/", RegistrationAPIView.as_view(), name="users-list"),
    path("users/login/", LoginAPIView.as_view(), name="user-login"),
    path("user/", UserRetrieveUpdateAPIView.as_view(), name="user-detail"),
]
