import json

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, force_authenticate, APIRequestFactory

from posts.models import Post
from posts.views import PostsViewSet, PostView, LikedPostsView
from authentication.models import User


class PostsTests(APITestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user(
            username="superprogrammer3000",
            email="email@example.com",
            password="Pas$w0rd",
        )
        self.posts_view = PostsViewSet.as_view(actions={"get": "list", "post": "create"})

    def test_post_creation(self):
        request = APIRequestFactory().post(
            reverse("posts:posts-list"),
            data={
                "content": "Some post content",
            },
            format="json"
        )
        force_authenticate(request, self.user)
        response = self.posts_view(request)
        response.render()
        resp_data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assert_(resp_data["post"]["content"] == "Some post content")
        self.assert_(resp_data["post"]["liked_by_user"] is False)
        self.assert_(resp_data["post"]["likes_count"] == 0)
        self.assert_(resp_data["post"]["author"]["username"] == "superprogrammer3000")
        request2 = APIRequestFactory().get(reverse("posts:posts-list"), format="json")
        force_authenticate(request2, self.user)
        response2 = self.posts_view(request2)
        response2.render()
        resp_data2 = json.loads(response2.content)
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assert_(len(resp_data2["posts"]) == 1)
        self.assert_(resp_data2["posts"][0]["liked_by_user"] is False)
        self.assert_(resp_data2["posts"][0]["likes_count"] == 0)
        self.assert_(resp_data2["posts"][0]["author"]["username"] == "superprogrammer3000")

    def test_post_creation_unauthorized(self):
        response = self.client.post(reverse("posts:posts-list"), data={"whatever": "whatever"}, format="json")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class PostTest(APITestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user(
            username="superprogrammer3000",
            email="email@example.com",
            password="Pas$w0rd",
        )
        self.user2 = User.objects.create_user(
            username="another_user",
            email="email2@example.com",
            password="Pas$w0rd2",
        )
        self.posts_view = PostsViewSet.as_view(actions={"get": "list", "post": "create"})
        self.post_view = PostView.as_view()
        self.liked_posts_view = LikedPostsView.as_view()

    def new_post(self):
        post = Post(content="Some post content", author=self.user)
        post.save()
        return post

    def test_post_like(self):
        post = self.new_post()
        request = APIRequestFactory().patch(
            reverse("posts:post-detail", args=[post.id]),
            data={
                "liked_by_user": True,
            },
            format="json"
        )
        force_authenticate(request, self.user2)
        response = self.post_view(request, pk=post.id)
        response.render()
        resp_data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assert_(resp_data["post"]["liked_by_user"] is True)
        self.assert_(resp_data["post"]["likes_count"] == 1)
        self.assert_(resp_data["post"]["id"] == post.id)
        request2 = APIRequestFactory().get(
            reverse("posts:liked"),
            format="json"
        )
        force_authenticate(request2, self.user2)
        response2 = self.liked_posts_view(request2)
        response2.render()
        resp_data2 = json.loads(response2.content)
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assert_(len(resp_data2["posts"]) == 1)
        self.assert_(resp_data2["posts"][0]["id"] == post.id)

    def test_post_get(self):
        self.new_post()
        request = APIRequestFactory().get(
            reverse("posts:post-detail", args=[1]),
            format="json"
        )
        force_authenticate(request, self.user2)
        response = self.post_view(request, pk=1)
        response.render()
        resp_data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assert_(resp_data["post"]["id"] == 1)
        self.assert_(resp_data["post"]["content"] == "Some post content")

