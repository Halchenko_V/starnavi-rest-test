from django.urls import path

from .views import PostView, PostsViewSet, LikedPostsView

app_name = "posts"


urlpatterns = [
    path("posts/", PostsViewSet.as_view(actions={"get": "list", "post": "create"}), name="posts-list"),
    path("post/<int:pk>", PostView.as_view(), name="post-detail"),
    path("posts/liked/", LikedPostsView.as_view(), name="liked"),
]
