import json

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, force_authenticate, APIRequestFactory

from authentication.views import UserRetrieveUpdateAPIView
from ..models import User


class UsersTests(APITestCase):
    def test_register(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse("authentication:users-list")
        data = {
            "user": {
                "email": "some_email@example.com",
                "username": "superprogrammer3000",
                "password": "1234567890",
            }
        }
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, "superprogrammer3000")

    def test_login(self):
        User.objects.create_user(
            username="superprogrammer3000",
            email="email@example.com",
            password="Pas$w0rd",
        )
        data = {"user": {"email": "email@example.com", "password": "Pas$w0rd"}}
        response = self.client.post(
            reverse("authentication:user-login"), data=data, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        resp_data = response.json()
        self.assertEqual(resp_data["user"]["email"], "email@example.com")
        self.assertEqual(resp_data["user"]["username"], "superprogrammer3000")
        self.assert_("token" in resp_data["user"])

    def test_retreive_user_fail(self):
        response = self.client.get(reverse("authentication:user-detail"), format="json")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retreive_user(self):
        user = User.objects.create_user(
            username="superprogrammer3000",
            email="email@example.com",
            password="Pas$w0rd",
        )
        request = APIRequestFactory().get(
            reverse("authentication:user-detail"), format="json"
        )
        force_authenticate(request, user)
        response = UserRetrieveUpdateAPIView.as_view()(request)
        response.render()
        resp_data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(resp_data["user"]["email"], "email@example.com")
        self.assertEqual(resp_data["user"]["username"], "superprogrammer3000")
        self.assertEqual(resp_data["user"]["bio"], "")
        self.assertEqual(resp_data["user"]["image"], "")
        self.assert_("token" in resp_data["user"])

    def test_update_user(self):
        user = User.objects.create_user(
            username="superprogrammer3000",
            email="email@example.com",
            password="Pas$w0rd",
        )
        request = APIRequestFactory().put(
            reverse("authentication:user-detail"),
            data={
                "user": {"username": "new_username", "email": "new_email@example.com"}
            },
            format="json",
        )
        force_authenticate(request, user)
        response = UserRetrieveUpdateAPIView.as_view()(request)
        response.render()
        resp_data = json.loads(response.content)
        self.assertEqual(resp_data["user"]["email"], "new_email@example.com")
        self.assertEqual(resp_data["user"]["username"], "new_username")
        self.assert_("token" in resp_data["user"])
