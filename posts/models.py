from django.db import models


class Post(models.Model):
    author = models.ForeignKey("authentication.User", on_delete=models.CASCADE)
    content = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    # likes_count = models.IntegerField(default=0)
    liked_by = models.ManyToManyField("authentication.User", related_name="liked_posts")
