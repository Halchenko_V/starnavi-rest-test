from rest_framework import serializers

from authentication.models import User
from authentication.serializers import UserSerializer
from .models import Post


class PostUserSerialzier(UserSerializer):
    class Meta:
        model = User
        fields = (
            "username",
            "profile",
            "bio",
            "image",
        )


class PostSerializer(serializers.ModelSerializer):
    author = PostUserSerialzier(read_only=True, required=False)
    liked_by_user = serializers.SerializerMethodField()
    likes_count = serializers.SerializerMethodField()

    class Meta:
        model = Post

        fields = (
            "id",
            "author",
            "content",
            "created_at",
            "likes_count",
            "liked_by_user",
        )
        read_only_fields = ("id", "created_at", "likes_count", "liked_by")

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(PostSerializer, self).get_validation_exclusions()
        return exclusions + ["author"]

    def get_liked_by_user(self, obj):
        return (
            Post.objects.get(pk=obj.id)
            .liked_by.filter(pk=self.context["request"].user.id)
            .exists()
        )

    def get_likes_count(self, obj):
        return Post.objects.get(pk=obj.id).liked_by.all().count()

    def update(self, instance, validated_data):
        request = self.context["request"]
        like = request.data.pop("liked_by_user", None)
        if like is True:
            instance.liked_by.add(request.user)
        elif like is False:
            instance.liked_by.remove(request.user)
        return super(PostSerializer, self).update(instance, validated_data)
