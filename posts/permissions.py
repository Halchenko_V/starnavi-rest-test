from rest_framework import permissions


class IsAuthorOfPost(permissions.BasePermission):
    message = "The user performing the request must be an author of the post"

    def has_object_permission(self, request, view, post):
        if request.user:
            return post.author == request.user
        return False
