from core.renderers import StarnaviJSONRenderer


class ProfileJSONRenderer(StarnaviJSONRenderer):
    object_label = "profile"
