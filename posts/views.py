from rest_framework import viewsets, permissions, generics

from posts.renderers import PostJSONRenderer, PostsJSONRenderer
from .permissions import IsAuthorOfPost
from .models import Post
from .serializers import PostSerializer


class PostsViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.order_by("-created_at")
    serializer_class = PostSerializer

    def get_renderers(self):
        if self.request.method == "POST":
            return [PostJSONRenderer()]
        return [PostsJSONRenderer()]

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        return permissions.IsAuthenticated(), IsAuthorOfPost()

    def perform_create(self, serializer):
        return serializer.save(author=self.request.user)

    def get_queryset(self):
        username = self.request.query_params.get("username", None)
        queryset = self.queryset
        if username is not None:
            queryset = queryset.filter(author__username=username)
        return queryset


class PostView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.order_by("-created_at")
    renderer_classes = (PostJSONRenderer,)

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        if self.request.method == "PATCH" and self.request.data in (
            {"liked_by_user": True},
            {"liked_by_user": False},
        ):
            return (permissions.IsAuthenticated(),)
        return permissions.IsAuthenticated(), IsAuthorOfPost()


class LikedPostsView(generics.ListAPIView):
    serializer_class = PostSerializer
    renderer_classes = (PostsJSONRenderer,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        return self.request.user.liked_posts.all().order_by("-created_at")
